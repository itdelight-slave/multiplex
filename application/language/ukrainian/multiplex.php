<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29.12.2015
 * Time: 2:04
 */
$lang['next-session-title'] = 'Найближчий сеанс:';
$lang['available-seats'] = 'Доступно місць:';
$lang['sessions-today'] = 'Сеанси сьогодні:';
$lang['buy-tickets'] = 'Купити квиток';
$lang['read-more'] = 'Детальніше';
$lang['now-cinema'] = 'Зараз у прокатi spmultiplex';
$lang['see-more'] = 'Показати всi';
$lang['soon-cinema'] = 'Скоро в прокатi spmultiplex';
$lang['other-session'] = 'Iншi сеанси';
$lang['session-details-price'] = 'Вартiсть:';
$lang['session-details-free-count'] = 'Вiльнi мiсця:';
$lang['buy-page'] = 'Пiдтвердити покупку';