<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29.12.2015
 * Time: 1:41
 */
$lang['next-session-title'] = 'Nearest browsing:';
$lang['available-seats'] = 'Available seats:';
$lang['sessions-today'] = 'Sessions today:';
$lang['buy-tickets'] = 'Buy tickets';
$lang['read-more'] = 'Read more';
$lang['now-cinema'] = 'Now in multiplex';
$lang['see-more'] = 'See more';
$lang['soon-cinema'] = 'Coming soon in Multiplex';
$lang['other-session'] = 'Other session';
$lang['session-details-price'] = 'Price:';
$lang['session-details-free-count'] = 'Free count:';
$lang['buy-page'] = 'Confirm the purchase';