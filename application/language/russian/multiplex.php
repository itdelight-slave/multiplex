<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 29.12.2015
 * Time: 1:55
 */
$lang['next-session-title'] = 'Ближайший сеанс:';
$lang['available-seats'] = 'Доступно мест:';
$lang['sessions-today'] = 'Сеансы сегодня:';
$lang['buy-tickets'] = 'Купить билет';
$lang['read-more'] = 'Подробнее';
$lang['now-cinema'] = 'Сейчас в прокате spmultiplex';
$lang['see-more'] = 'Показать все';
$lang['soon-cinema'] = 'Скоро в прокате spmultiplex';
$lang['other-session'] = 'Другие сеансы';
$lang['session-details-price'] = 'Стоимость:';
$lang['session-details-free-count'] = 'Свободные места:';
$lang['buy-page'] = 'Подтвердить покупку';