<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset=utf-8" />
    <!--[if IE]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <![endif]-->
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" /> -->
    <title>multiplex</title>
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <link href="./assets/images/favicon.png" rel="shortcut icon" type="image/x-icon" />
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300' rel='stylesheet' type='text/css'>
    <link href="./assets/css/slick.css" rel="stylesheet" type="text/css" />
    <link href="./assets/css/style.css" rel="stylesheet" type="text/css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!-- slider section -->
<section id="" class="">
    <div class="main-slider">
        <? foreach ($slider as $itemSlide): ?>
            <div class="slide" style="background-image:url('<?=$itemSlide['generalPoster']['path'];?>')">
            <div class="right-side">
                <header>
                    <h2 class="title"><?=$itemSlide['name'];?>: </h2>
                    <h4 class="subtitle"><?=$itemSlide['subname'];?></h4>
                </header>
                <div class="score-row">
                    <div class="age"><?=$itemSlide['age'];?>+</div>

                    <!-- add classes: '_one_star' or '_two_star' or '_three_star' or '_four_star' or '_five_star'-->
                    <div class="score-stars _<?=$itemSlide['rating_value'];?>_star">
                        <span class="star-five"></span>
                        <span class="star-five"></span>
                        <span class="star-five"></span>
                        <span class="star-five"></span>
                        <span class="star-five"></span>
                    </div>
                </div>
                <h6 class="next-session-title">Ближайший сеанс:</h6>
                <div class="session-time"><?=$itemSlide['seanses']['firstActive']['time'];?></div>
                <div class="available-seats">Доступно мест: <span><?=$itemSlide['seanses']['firstActive']['free_count'];?> из <?=$itemSlide['seanses']['firstActive']['tikets_count'];?></span></div>
                <div class="row">
                    <h6 class="sessiontoday">Сеансы сегодня: </h6>
                    <ul class="session-time-list">
                        <? foreach ($itemSlide['seanses']['all'] as $itemSeans): ?>
                            <li<?=$itemSeans['isActive'] ? '' : ' class="_not-active"';?>><?=$itemSeans['time'];?></li>
                        <? endforeach; ?>
                    </ul>
                </div>

                <div class="row">
                    <button class="btn btn-primary buy-tickets-btn" ">Купить билет</button>
                    <button class="btn btn-white inverse" onclick="location.href='<?=$itemSlide['link']['info_page'];?>';">Подробнее</button>
                </div>
            </div>
        </div>
        <? endforeach; ?>
    </div>
</section>
<!-- slider section end -->

<section class="films-list-section">
    <div class="container">
        <h3>Сейчас в прокате spmultiplex <a class="see-more" href="#">Показать все <i class="icon"></i></a></h3>
        <div class="films-list">
            <? foreach ($nowTheaters as $itemNowTheaters): ?>
                <a class="list-item" href="<?=$itemNowTheaters['link']['info_page'];?>">
                    <div class="thumbnail">
                        <div class="flag">
                            <i class="icon"></i>
                            <span class="cup-num">145</span>
                        </div>
                        <img src="<?=$itemNowTheaters['generalPoster']['path'];?>" alt="<?=$itemNowTheaters['name'];?>">
                    </div>
                    <h5><?=$itemNowTheaters['name'];?></h5>
                    <h6><?=$itemNowTheaters['original_name'];?>, <?=$itemNowTheaters['year'];?></h6>
                </a>
            <? endforeach; ?>
        </div>

        <h3>Скоро в прокате spmultiplex <a class="see-more" href="#">Показать все <i class="icon"></i></a></h3>
        <div class="films-list">
            <? foreach ($anons as $itemAnons): ?>
            <a class="list-item" href="<?=$itemAnons['link']['info_page'];?>">
                <div class="thumbnail">
                    <div class="flag">
                        <i class="icon"></i>
                        <span class="cup-num">145</span>
                    </div>
                    <img src="<?=$itemAnons['generalPoster']['path'];?>" alt="<?=$itemAnons['name'];?>">
                </div>
                <h5><?=$itemAnons['name'];?></h5>
                <h6><?=$itemAnons['original_name'];?>, <?=$itemAnons['year'];?></h6>
            </a>
            <? endforeach; ?>
        </div>
    </div>
</section>

<div class="popup buy-tickets">
    <div class="popup-body">
        <button class="close-popup"></button>
        <div class="thumbnail">
            <div class="flag">
                <i class="icon"></i>
                <span class="cup-num">145</span>
            </div>
            <img src="../../../assets/images/film-3.jpg" alt="">
        </div>
        <h2>Звездные войны: Пробуждение силы</h2>
        <h5>The Star Wars: The Force Awakens</h5>
        <div class="session-details">
            Время сеанса:
            <span>13:00</span>
            <button class="btn btn-white btn-sm inverse">Другие сеансы</button>
        </div>
        <div class="session-details">Стоимость:<span> 50 баллов</span></div>
        <div class="session-details">Свободные места:<span>3 из 10</span></div>

        <button class="btn btn-primary" onclick="location.href='<?=$itemSlide['link']['buy_page'];?>';">Подтвердить покупку</button>
    </div>
</div>
</div>

<script type="text/javascript" src="../../../assets/js/jquery.min.js"></script>
<script type="text/javascript" src="../../../assets/js/slick.min.js"></script>
<script type="text/javascript" src="../../../assets/js/main.js"></script>


</body>
</html>
