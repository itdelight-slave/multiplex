<html>
<body>
  <tr>
    <td>Reviews</td>
  </tr>
    <H1>Create record</H1>
    <?php echo form_open('review/addRecord'); ?>
    <p>
        <label for="title">Title</label>
        <input type="text" title="title" id="title"/>
    </p>
    <p>
        <label for="comments">Comments</label>
        <input type="text" title="comments" id="comments">
    </p>
    <p>
        <input type="submit" value="submit" />
    </p>
    <?php echo form_close(); ?>
    <hr />
  <h2>Read</h2>
  <?php if(isset($records)) : foreach ($records as $row): ?>
  <h2><?php echo anchor("model_review/deleteRecord/$row->id" , $row->title); ?></h2>
  <div><?php echo $row->review; ?></div>
  <?php endforeach; ?>
  <?php else : ?>
  <h2>No records for this.</h2>
  <?php endif; ?>

</body>
</html>