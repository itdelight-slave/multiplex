<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Flare Video</title>
    <link rel="stylesheet" href="/play/maccman-flarevideo-1.1-0-gee31f65/maccman-flarevideo-ee31f65/stylesheets/flarevideo.css" type="text/css">
    <link rel="stylesheet" href="/play/maccman-flarevideo-1.1-0-gee31f65/maccman-flarevideo-ee31f65/stylesheets/flarevideo.default.css" type="text/css">
    <script src="/play/maccman-flarevideo-1.1-0-gee31f65/maccman-flarevideo-ee31f65/javascripts/jquery.js" type="text/javascript"></script>
    <script src="/play/maccman-flarevideo-1.1-0-gee31f65/maccman-flarevideo-ee31f65/javascripts/jquery.ui.slider.js" type="text/javascript"></script>
    <script src="/play/maccman-flarevideo-1.1-0-gee31f65/maccman-flarevideo-ee31f65/javascripts/jquery.flash.js" type="text/javascript"></script>
    <script src="/play/maccman-flarevideo-1.1-0-gee31f65/maccman-flarevideo-ee31f65/javascripts/flarevideo.js" type="text/javascript"></script>
    <script type="text/javascript" charset="utf-8">
        jQuery(function($){
            fv = $("#video").flareVideo();
            fv.load([
                {
                    src: 'http://<?=$base_url;?>/download/<?=$film['id'] . '/' . $seansId;?>',
                    type: '<?=$film['codec'];?>'
                }
            ]);
        })
    </script>
    <style type="text/css" media="screen">
        body {
            background: #27282C url(/play/maccman-flarevideo-1.1-0-gee31f65/maccman-flarevideo-ee31f65/images/bg.png) repeat;
        }

        #video {
            -webkit-box-shadow: 0 0 20px #000;
            -moz-box-shadow: 0 0 20px #000;
            width: 900px;
            height: 500px;
            overflow: none;
            margin: 5% auto;
        }
    </style>
</head>
<body>
<div id="video"></div>
</body>
</html>