<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * File factory class.
 */
class FileFactory
{
    /**
     * @constructor
     */
    public function __construct()
    {
        require_once __DIR__ . '/FileFactory/poster.php';
        require_once __DIR__ . '/FileFactory/film.php';
    }

    /**
     * Create file.
     *
     * @param array $data
     * @return IFile
     */
    public function createFile(array $data)
    {
        $file = null;
        switch ($data['file_type_id']) {
            case 1:
            case 2:
                $file = new poster($data);
                break;
            case 6:
                $file = new film($data);
        }

        return $file;
    }
} 