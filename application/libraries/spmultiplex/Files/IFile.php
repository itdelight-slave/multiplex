<?php
/**
 * Created by PhpStorm.
 * User: windows
 * Date: 22.12.2015
 * Time: 21:30
 */

/**
 * File interface.
 */
interface IFile
{
    /**
     * Get file data.
     *
     * @return array
     */
    public function getData();
} 