<?php
/**
 * Created by PhpStorm.
 * User: windows
 * Date: 22.12.2015
 * Time: 21:24
 */

require_once __DIR__ . '/../IFile.php';
require_once __DIR__ . '/../File.php';

/**
 * Poster file classs.
 */
class Poster extends File implements IFile
{
    /**
     * Get poster date.
     *
     * @return array
     */
    public function getData()
    {
        $data = ['path' => IMAGE_DIR . $this->data['name'] . '.' . $this->data['mime_type']];
        if ($this->data['file_type_id'] == 2) {
            $data['isGeneralPost'] = true;
        }

        return $data;
    }
} 