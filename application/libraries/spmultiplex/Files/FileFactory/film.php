<?php
/**
 * Created by PhpStorm.
 * User: windows
 * Date: 22.12.2015
 * Time: 21:24
 */

require_once __DIR__ . '/../IFile.php';
require_once __DIR__ . '/../File.php';

/**
 * Film file classs.
 */
class Film extends File implements IFile
{
    /**
     * List of codecs.
     *
     * @var array
     */
    protected $codecs = [
        'mp4' => 'video/mp4'
    ];

    /**
     * Get film date.
     *
     * @return array
     */
    public function getData()
    {
        return [
            'film_id' => $this->data['film_id'],
            'path' => UPLOAD_DIR . $this->data['name'] . '.' . $this->data['mime_type'],
            'local_path' => 'D:/xampp/htdocs/sp/www/multiplex' . UPLOAD_DIR . $this->data['name'] . '.' . $this->data['mime_type'],
            'codec' => $this->codecs[$this->data['mime_type']]
        ];
    }
} 