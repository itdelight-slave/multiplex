<?php
/**
 * Created by PhpStorm.
 * User: windows
 * Date: 22.12.2015
 * Time: 21:30
 */

/**
 * File interface.
 */
abstract class File
{
    /**
     * Controller class.
     *
     * @var Controller
     */
    protected $CI;

    /**
     * Data array.
     *
     * @var array
     */
    protected $data;

    /**
     * @constructor
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
        $this->CI = &get_instance();
    }
} 