<?php
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Cron class.
 */
class cron extends SP_Controller
{
    /**
     * @constructor
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('spmultiplex/model_file', 'file', TRUE);
        $this->load->model('spmultiplex/model_seans', 'seans', TRUE);
        $this->load->model('spmultiplex/model_user', 'user');
    }

    /**
     * Film run.
     *
     * @return void
     */
    public function runFilm() {
        $seanses = $this->seans->getNowSeanses();
    }
}