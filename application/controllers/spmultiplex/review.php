<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}
/**
 * review class
 */
class Review extends SP_Controller
{
    /**
     * @constructor
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('spmultiplex/model_reviews', 'model_reviews', TRUE);
        $this->load->helper('url');
        $this->load->model('spmultiplex/model_user', 'model_user', TRUE);

    }
    public function index()
    {
        $data = array();
        if($query = $this->model_reviews->getRecord())
        {
            $data['records'] = $query;
        }
        $this->load->view('reviews', $data);
    }
     /**
     *
     * @return void
     */
    public function add()
    {
        $data = array(
            'title' => $this->input->post('title'),
            'review' => $this->input->post('review')
        );
        $this->model_reviews->addRecord($data);
        $this->index();
    }
    public function update()
    {
        if($this->model_user->getId() == 1) {
            $data = array(
                'reviews' => 'reviews', // pass the real table name
                'title' => $this->input->post('title'),
                'review' => $this->input->post('review')
            );

            $this->load->model('model_reviews'); // load the model first
            if ($this->model_reviews->updateRecord($data)) // call the method from the model
            {
                $this->load->view('reviews', $data);
            }
        }
        else
        {
            return;
        }
    }
    public function delete()
    {
        if($this->model_user->getId() == 1) {
            $this->model_reviews->deleteRecord();
            $this->index();
        }
        else {
            return;
        }
    }
}