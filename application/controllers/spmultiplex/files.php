<?php
session_start();
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * File class.
 */
class files extends SP_Controller
{
    /**
     * @constructor
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('spmultiplex/model_file', 'file', TRUE);
        $this->load->model('spmultiplex/model_user', 'user');
    }

    /**
     * Get user ip.
     *
     * @return string
     */
    private static function getIp()
    {
        return @$_SERVER['HTTP_X_FORWARDED_FOR'] ?: @$_SERVER['REMOTE_ADDR'] ?: '';
    }

    /**
     * Get user film file key.
     *
     * @param int $fileId
     * @return string
     */
    protected function getUserFilmFileKey($fileId)
    {
        return __METHOD__ . '_' . self::getIp() . '_' . $this->user_id . '_' . $fileId;
    }

    /**
     * Play.
     *
     * @param int $fileId
     * @param int $seansId
     * @return mixed
     */
    public function play($fileId, $seansId)
    {
        //TODO ������������ ��� ���������
        $data = $this->file->getFilm($fileId);
        $this->checkFile($seansId, $data);
        $fm = fopen($data['local_path'], 'rb');
        list($begin, $end, $size, $part) = $this->getRange($data['local_path']);
        //TODO uncoment
//        $this->checkDoubleConnection($fileId, $begin, $fm);
        $this->setHeaders($begin, $end, $size, $data);
        $this->fileSend($begin, $end, $part, $fm);
        //TODO uncoment
//        $this->cache->save($this->user->getId(), ['player' => [$fileId => $cur]]);
        fclose($fm);
        die();
    }

    /**
     * File send.
     *
     * @param int $begin
     * @param int $end
     * @param int $part
     * @param resource $fm
     * @return void
     */
    protected function fileSend($begin, $end, $part, $fm) {
        $cur = $begin;
        fseek($fm, $begin, 0);
        while (!feof($fm) && $cur < $end && (connection_status() == 0)) {
            print fread($fm, min($part, $end - $cur));
            $cur += $part;
//            usleep(1000); //����� ������������ ��������
        }
    }

    /**
     * Check file.
     *
     * @param int $seansId
     * @param array $data
     * @return void
     */
    protected function checkFile($seansId, array $data) {
        if (isset($data['film_id'])) {
            $this->user->checkTicket(md5($data['film_id'] . $seansId . $this->user->getId()));
        } else {
            show_error('Film is not found in system.');
        }
        if (!isset($data['local_path'])) {
            show_error('Bad file format.');
        }
        if (!file_exists($data['local_path'])) {
            header("HTTP/1.0 404 Not Found");
            die();
        }
    }

    /**
     * Get range of file.
     *
     * @param string $path
     * @return array
     */
    protected function getRange($path) {
        $begin = 0;
        $part = 1024 * 16; // ��� ����� ������ ��������, ����� ���� ������������
        $size = filesize($path);
        $end = $size;
        if (isset($_SERVER['HTTP_RANGE'])) {
            if (preg_match('/bytes=\h*(\d+)-(\d*)[\D.*]?/i', $_SERVER['HTTP_RANGE'], $matches)) {
                $begin = intval($matches[0]);
                if (!empty($matches[1])) {
                    $end = intval($matches[1]);
                }
            }
        }

        return [$begin, $end, $size, $part];
    }

    /**
     * Set headers.
     *
     * @param int $begin
     * @param int $end
     * @param int $size
     * @param array $data
     * @return void
     */
    protected function setHeaders($begin, $end, $size, array $data) {
        if ($begin > 0 || $end < $size) {
            header('HTTP/1.0 206 Partial Content');
        } else {
            header('HTTP/1.0 200 OK');
        }
        header("Content-Type: " . $data['codec']);
        header('Accept-Ranges: bytes');
        header('Content-Length:' . ($end - $begin));
        header("Content-Disposition: inline;");
        header("Content-Range: bytes $begin-$end/$size");
        header("Content-Transfer-Encoding: binary\n");
        header('Connection: close');
    }

    /**
     * Check on double connection.
     *
     * @param int $fileId
     * @param int $begin
     * @param resource
     * @return void
     */
    protected function checkDoubleConnection($fileId, $begin, $fm)
    {
        $cache = $this->cache->get($this->user->getId());
        if (!empty($cache) && isset($cache['player'][$fileId])) {
            if ($begin < $cache['player'][$fileId]['lastByte']) {
                show_error('Double Connection.');
                fclose($fm);
                die();
            }
        }
    }
}