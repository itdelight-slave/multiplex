<?php if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * Sp multiplex class.
 */
class spmultiplex extends SP_Controller
{
    /**
     * @constructor
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('spmultiplex/model_films', 'films', TRUE);
        $this->load->model('spmultiplex/model_user', 'user');
    }

    /**
     * General Page.
     *
     * @return void
     */
    public function index() {
        $this->todayFilms();
    }

    /**
     * Get today films.
     *
     * @return void
     */
    public function todayFilms()
    {
        $data = [
            'slider' => $this->films->getTodayFilms(5),
            'nowTheaters' => $this->films->getNowTheaters(5),
            'anons' => $this->films->getAnonses(5)
        ];
        $this->load->view('spmultiplex/today_films', $data);
    }

    /**
     * Film player.
     *
     * @param int $filmId
     * @param int $seansId
     * @return void
     */
    public function filmPlayer($filmId, $seansId)
    {
        if (!$filmId || !$seansId) {
            show_error('Mandatory params isn\' set.');
        }
        $data = $this->films->getFilmPlayer($filmId, $seansId, $this->user->getId());
        $data['base_url'] = $this->config->item('base_url');
        $data['seansId'] = $seansId;
        $this->load->view('spmultiplex/film_player', $data);
    }

    /**
     * Now in theaters.
     *
     * @return void
     */
    public function nowTheaters()
    {
        $data = [
            'premiera' => $this->films->getPremiera(),
            'nowTheaters' => $this->films->getNowTheaters()
        ];
        $this->load->view('spmultiplex/now_theaters', $data);
    }

    /**
     * Get films on this weekend.
     *
     * @return void
     */
    public function weekend()
    {
        $data = $this->films->getWeekend();
        $this->load->view('spmultiplex/weekend', ['data' => $data]);
    }

    /**
     * Film page.
     *
     * @param int $filmId
     * @return void
     */
    public function film($filmId) {
        $data = $this->films->getFilm(['wheres' => [['type' => 'AND', 'where' => ['`films`.`id`' => $filmId]]]]);
        $this->load->view('spmultiplex/film', ['film' => $data]);
    }
}