<?php
session_start();
if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

/**
 * User class.
 */
class users extends SP_Controller
{
    /**
     * @constructor
     */
    function __construct()
    {
        parent::__construct();
        $this->load->model('spmultiplex/model_user', 'user');
        $this->load->model('spmultiplex/model_films', 'films', TRUE);
    }

    /**
     * Ticket buy.
     *
     * @param int $filmId
     * @param int $seansId
     */
    public function ticketBuy($filmId, $seansId)
    {
        $data = [
            'film' => $this->films->getFilm(['wheres' => ['type' => 'AND', 'where' => ['films.id' => $filmId]]]),
            'tickesBuyData' => $this->user->ticketBuy($seansId)
        ];
        $this->load->view('spmultiplex/film', $data);
    }
}