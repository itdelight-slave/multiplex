<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Film model.
 */
class model_file extends CI_Model
{
    /**
     * Get film.
     *
     * @param int $fileId
     * @return mixed
     */
    public function getFilm($fileId)
    {
        $this->db->select('*')->from('files')->where('id', $fileId);
        $query = $this->db->get();
        $row = $query->row_array();
        if (empty($row)) {
            show_error('File not found.');
        }
        $this->load->library('spmultiplex/Files/FileFactory');
        $data = $this->filefactory->createFile($row)->getData();

        return $data;
    }
}