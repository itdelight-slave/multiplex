<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class model_reviews extends CI_Model {

    /**
     * @return mixed
     */
        public function getRecord()
    {
        $query = $this->db->get('reviews');
        return $query->result();
    }
    /**
     * @param $data
     * @return mixed
     */
    public function addRecord($data) {
        $this->db->insert("reviews" , $data);
        return;
    }

    /**
     * @param $data
     * @return bool
     */
    public function updateRecord($data) {
        $this->db->where('id', $this->uri->segment(3));
        $this->db->update('reviews', $data);
    }

    /**
     * @return mixed
     */
    public function deleteRecord() {
        $this->db->where('id', $this->uri->segment(3));
        $this->db->delete('reviews');
    }
}