<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Seans model.
 */
class model_seans extends CI_Model
{
    const STATUS_NOT_BEGIN = 0;
    const STATUS_PLAY = 1;
    const STATUS_ENDED = -1;

    /**
     * Get seans.
     *
     * @param int $seansId
     * @return array
     */
    public function getSeans($seansId)
    {
        $this->db->select('*')->from('seans')->where('seans.id', $seansId);
        $query = $this->db->get();
        $seans = $query->row_array();
        if (!empty($seans)) {
            $this->checkSeans($seans);
            return $seans;
        } else {
            show_error('Seans is not found.');
            exit;
        }
    }

    /**
     * Check seans.
     *
     * @param array $data
     * @return void
     */
    protected function checkSeans(array $data)
    {
        if ($data['status'] == -1) {
            show_error('Seans is ended.');
        }
        if ($data['free_count'] == 0) {
            show_error('Seans is over stack.');
        }
    }

    /**
     * Decrement free count.
     *
     * @param array $data
     * @return void
     */
    public function decrementFreeCount(array $data)
    {
        $this->db->set('free_count', 'free_count - 1', FALSE);
        $this->db->where('id', $data['id']);
        $this->db->update('seans');
    }

    /**
     * Get now seanses.
     *
     * @return array
     */
    public function getNowSeanses()
    {
        $this->db->trans_start();
        $date = date('Y-m-d H:i');
        $this->db->select('*')->from('seans')
            ->where(['date_time_seans' => $date, 'status' => self::STATUS_NOT_BEGIN]);
        $query = $this->db->get();
        $seanses = $query->result_array();
        foreach ($seanses as $itemSeans) {
            $this->db->update('seans', ['status' => self::STATUS_PLAY], ['id' => $itemSeans]);
        }
        $this->db->trans_complete();

        return $seanses;
    }
}