<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once __DIR__ . '/model_seans.php';

/**
 * Film model.
 */
class model_films extends CI_Model
{
    /**
     * Controller class.
     *
     * @var Controller
     */
    protected $CI;

    /**
     * Constructor
     *
     * @access public
     */
    function __construct()
    {
        parent::__construct();
        $this->CI = &get_instance();
        $this->CI->load->model('spmultiplex/model_rating', 'rating', TRUE);
    }

    /**
     * Get today films.
     *
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function getTodayFilms($limit = null, $offset = null)
    {
        $date = date('Y-m-d H:i:s');
        $queryParams = [
            'wheres' => [
                [
                    'type' => 'AND',
                    'where' => ['seans.date_time_seans >=' => $date, 'films.date_premiere <=' => $date, 'films.date_end_translation >=' => $date]
                ]
            ]
        ];
        return $this->getFilms($queryParams, $limit, $offset, true, true, true, false, false, false, false, true, false, date('Y-m-d'));
    }

    /**
     * Get now at theatres.
     *
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function getNowTheaters($limit = null, $offset = null)
    {
        $date = date('Y-m-d H:i:s');
        $queryParams = [
            'wheres' => [
                [
                    'type' => 'AND',
                    'where' => ['films.date_premiere <=' => $date, 'films.date_end_translation >=' => $date]
                ]
            ]
        ];
        return $this->getFilms($queryParams, $limit, $offset);

    }

    /**
     * Get anonses.
     *
     * @param int|null $limit
     * @param int|null $offset
     * @return array
     */
    public function getAnonses($limit = null, $offset = null)
    {
        $date = date('Y-m-d H:i:s');
        $queryParams = [
            'wheres' => [
                [
                    'type' => 'AND',
                    'where' => ['films.date_premiere >=' => $date]
                ]
            ]
        ];
        return $this->getFilms($queryParams, $limit, $offset);
    }

    /**
     * Get premiera
     * @return array
     */
    public function getPremiera()
    {
        $this->db->order_by('id', 'RANDOM');
        $this->db->limit(1);
        $query = $this->db->get('films');
        return $query->result_array();

    }

    /**
     * Get film data.
     *
     * @param int $filmId
     * @param int $seansId
     * @param int $userId
     * @return array
     */
    public function getFilmPlayer($filmId, $seansId, $userId)
    {
        $queryParams = [
            'wheres' => [
                [
                    'type' => 'AND',
                    'where' => [
                        'films.id' => $filmId,
                        'seans.status' => model_seans::STATUS_PLAY,
                        'tickets.token' => md5($filmId . $seansId . $userId)
                    ]
                ]
            ],
            'joins' => [
                [
                    'type' => 'inner',
                    'join' => ['tickets', 'tickets.seans_id = seans.id']
                ]
            ]
        ];
        return $this->getFilm($queryParams, null, null, false, true, false, false, false, false, true);
    }

    /**
     * Get weekend films.
     *
     * @return array
     */
    public function getWeekend()
    {
        $result = [];
        $queryParams = [
            'wheres' => [
                [
                    'type' => 'AND',
                    'where' => [
                        'seans.date_time_seans >=' => date('Y-m-d H:i:s', strtotime("last Thursday")),
                        'seans.date_time_seans <=' => date('Y-m-d H:i:s', strtotime("next Thursday"))
                    ]
                ]
            ]
        ];
        $data = $this->getFilms($queryParams, null, null, false, true, false, false, false, false, false);
        foreach ($data as $item) {
            foreach ($item['seanses'] as $itemSeans) {
                $result[date('d F, l', strtotime($itemSeans['date_time_seans']))][$item['film_id']] = $item;
            }
        }

        return $result;
    }

    /**
     * Get film.
     *
     * @param array $queryParams
     * @param null $limit
     * @param null $offset
     * @param bool $info
     * @param bool $seanses
     * @param bool $posters
     * @param bool $cadres
     * @param bool $soundtracs
     * @param bool $trailers
     * @param bool $film
     * @param bool $link
     * @return array
     */
    public function getFilm(array $queryParams, $limit = null, $offset = null, $info = true, $seanses = true, $posters = true, $cadres = false, $soundtracs = false, $trailers = false, $film = false, $link = true)
    {
        $films = $this->getFilms($queryParams, $limit, $offset, $info, $seanses, $posters, $cadres, $soundtracs, $trailers, $film, $link);

        if (isset($films[0])) {
            return $films[0];
        } else {
            show_error('Film not found.');
            exit;
        }
    }

    /**
     * Get films.
     *
     * @param array $queryParams
     * @param int|null $limit
     * @param int|null $offset
     * @param bool $info
     * @param bool $seanses
     * @param bool $posters
     * @param bool $cadres
     * @param bool $soundtracs
     * @param bool $trailers
     * @param bool $film
     * @param bool $link
     * @param bool $review
     * @param date $date
     * @return array
     */
    public function getFilms($queryParams, $limit = null, $offset = null, $info = true, $seanses = true, $posters = true, $cadres = false, $soundtracs = false, $trailers = false, $film = false, $link = true, $review = false, $date = null)
    {
        $this->db->select('*, films.id as film_id')->from('films');
        $this->db->join('seans', 'seans.film_id = films.id');
        if ($info) {
            $this->db->join('film_info', 'film_info.id = films.film_info_id');
        }
        if (!empty($queryParams)) {
            if (isset($queryParams['wheres'])) {
                foreach ($queryParams['wheres'] as $itemWhere) {
                    if (isset($itemWhere['type']) && isset($itemWhere['where'])) {
                        switch ($itemWhere['type']) {
                            case 'AND':
                                $this->db->where($itemWhere['where']);
                                break;
                        }
                    }
                }
            }
            if (isset($queryParams['joins'])) {
                foreach ($queryParams['joins'] as $itemJoin) {
                    if (isset($itemJoin['type']) && isset($itemJoin['join'])) {
                        switch ($itemJoin['type']) {
                            case 'inner':
                                $this->db->join($itemJoin['join'][0], $itemJoin['join'][1], $itemJoin['type']);
                                break;
                        }
                    }
                }
            }
        }
        if (null !== $limit) {
            $offset = ($offset !== null) ? $offset : 0;
            $this->db->limit($limit, $offset);
        }
        $this->db->group_by('films.id');
        $query = $this->db->get();
        $result = $query->result_array();
        $result = $this->prepareFilmsData($result, $seanses, $posters, $cadres, $soundtracs, $trailers, $film, $link, $review, $date);

        return $result;
    }

    /**
     * Prepare films data.
     *
     * @param array $data
     * @param bool $seanses
     * @param bool $posters
     * @param bool $cadres
     * @param bool $soundtracs
     * @param bool $trailers
     * @param bool $film
     * @param bool $link
     * @param bool $review
     * @param date $date
     * @return array
     */
    protected function prepareFilmsData(array $data, $seanses, $posters, $cadres, $soundtracs, $trailers, $film, $link, $review, $date)
    {
        foreach ($data as $key => $item) {
            if ($seanses) {
                $data[$key]['seanses'] = $this->getSeanses($item, $date);
            }
            $postarsList = $this->preparePosters($item);
            $data[$key]['generalPoster'] = array_pop($postarsList);
            if ($posters) {
                $data[$key]['posters'] = $postarsList;
            }
            if ($film) {
                $data[$key]['film'] = $this->prepareFilm($item);
            }
            if ($link) {
                $data[$key]['link'] = $this->getLink($data[$key]);
            }
            if ($review) {
                $data[$key]['reviews'] = $this->getReviews($item);
            }
            $data[$key]['rating_value'] = $this->CI->rating->prepareRatingFormat($data[$key]['rating_value']);

        }

        return $data;
    }

    /**
     * Prepare film data.
     *
     * @param array $data
     * @return array
     */
    protected function prepareFilm(array $data)
    {
        $this->db->select('*')->from('files')
            ->where('film_id', $data['id'])
            ->where('file_type_id', 6);
        $query = $this->db->get();
        $result = $query->row_array();
        $data = $this->prepareFiles([$result]);

        if (isset($data[0])) {
            return array_merge($result, $data[0]);
        } else {
            show_error('Bad file format.');
            exit;
        }
    }

    /**
     * Gt link for film.
     *
     * @param array $data
     * @return array
     */
    protected function getLink(array $data)
    {
        return [
            'info_page' => '/spmultiplex/spmultiplex/film/' . $data['id'],
            'buy_page' => '/spmultiplex/users/ticketBuy/' . $data['id'] . '/' . $data['seanses']['firstActive']['id']
        ];
    }


    /**
     * Get seanses data.
     *
     * @param array $data
     * @param date $date
     * @return array
     */
    protected function getSeanses(array $data, $date)
    {
        $where = ['film_id' => $data['id']];
        if (null !== $date) {
            $where['date_time_seans REGEXP '] = "$date";
        }
        $this->db->select('*')
            ->from('seans')
            ->where($where)
            ->order_by('date_time_seans');
        $query = $this->db->get();
        $data = $query->result_array();
        return $this->prepareSeanses($data);
    }
    /**
     * Get seanses data.
     *
     * @param array $data
     * @return array
     */
    protected function getReviews(array $data)
    {
        $this->db->select('*')->from('reviews')->where('reviews.review', $data['id']);
        $query = $this->db->get();
        $data = $query->result_array();
        return $this->prepareReviews($data);
    }

    /**
     * Prepare seanses data.
     *
     * @param array $data
     * @return array
     */
    protected function prepareSeanses(array $data)
    {
        $firstActive = null;
        foreach ($data as $key => $item) {
            $data[$key]['time'] = $this->prepareTime($item);
            $data[$key]['isActive'] = $item['date_time_seans'] > date('Y-m-d H:i:s');
            $data[$key]['isOverStack'] = $item['free_count'] == 0;
            $firstActive = ($data[$key]['isActive'] && null === $firstActive) ? $data[$key] : $firstActive;
        }

        return ['all' => $data, 'firstActive' => $firstActive];
    }
    /**
     * Prepare reviews data.
     * @param array $data
     * @return array
     */
    protected function prepareReviews(array $data)
    {
        foreach ($data as $key => $item ) {
            $data['comment'] = $item['review'];
//            $data[$key]['time'] = $this->prepareTime($item);
//            $data[$key]['isActive'] = $item['date_time_seans'] > date('Y-m-d H:i:s');
//            $data[$key]['isOverStack'] = $item['free_count'] == 0;
        }

        return $data;
    }

    /**
     * Prepare time.
     *
     * @param array $data
     * @return string
     */
    protected function prepareTime(array $data)
    {
        preg_match('/.* (.*):\d\d/', $data['date_time_seans'], $match);
        return isset($match[1]) ? $match[1] : null;
    }

    /**
     * Prepare posters.
     *
     * @param $data
     * @return array
     */
    protected function preparePosters(array $data)
    {
        $this->db->select('*')->from('files')
            ->where("film_id = {$data['id']} AND (file_type_id = 1 OR file_type_id = 2)")
            ->order_by('file_type_id');
        $query = $this->db->get();
        $result = $query->result_array();

        return $this->prepareFiles($result);
    }

    /**
     * Prepare film data.
     *
     * @param array $data
     * @return array
     */
    protected function prepareFiles(array $data)
    {
        foreach ($data as $key => $item) {
            $this->load->library('spmultiplex/Files/FileFactory');
            $data[$key] = $this->filefactory->createFile($item)->getData();
        }

        return $data;
    }
}