<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Tickets model.
 */
class model_tickets extends CI_Model
{
    /**
     * Create ticket in system.
     *
     * @param array $seansData
     * @param int $userId
     * @return void
     */
    public function create(array $seansData, $userId)
    {
        $data = [
            'user_id' => $userId,
            'seans_id' => $seansData['id'],
            'date_buy' => date('Y-m-d H:i:s'),
            'token' => md5($seansData['film_id'] . $seansData['id'] . $userId)
        ];

        $this->db->insert('tickets', $data);
    }
}