<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Film model.
 */
class model_user extends CI_Model
{
    /**
     * Controller class.
     *
     * @var Controller
     */
    protected $CI;

    /**
     * Constructor
     *
     * @access public
     */
    function __construct()
    {
        parent::__construct();
        $this->CI = &get_instance();
        $this->CI->load->model('spmultiplex/model_seans', 'seans', TRUE);
        $this->CI->load->model('spmultiplex/model_tickets', 'tickets', TRUE);
    }

    public function getId()
    {
        return 1;
    }

    public function getName()
    {
        return 'Witaha';
    }

    /**
     * @return float
     */
    public function getBalance()
    {
        return 1000;
    }

    public function getUserRole()
    {
        return 'viewer'; //admin
    }

    public function getTickets()
    {

    }

    /**
     * Ticket buy.
     *
     * @param int $seansId
     * @return array
     */
    public function ticketBuy($seansId)
    {
        $seans = $this->CI->seans->getSeans($seansId);
        $this->checkSeansData($seans);
        $this->buyProcess($seans);

        return [
            'status' => 'complete'
        ];
    }

    /**
     * Ticket buy process.
     *
     * @param array $seans
     * @return void
     */
    protected function buyProcess(array $seans)
    {
        $this->pay($seans['price']);
        $this->CI->seans->decrementFreeCount($seans);
        $this->CI->tickets->create($seans, $this->getId());
    }

    /**
     * Pay.
     *
     * @param float $amount
     * @return void
     */
    protected function pay($amount)
    {
        $belence = $this->getBalance() - $amount;
    }

    /**
     * Check seans data.
     *
     * @param array $seans
     * @return void
     */
    protected function checkSeansData(array $seans)
    {
        if ($this->getBalance() < $seans['price']) {
            show_error('You don\'t have enough funds to purchase.');
        }
    }

    /**
     * Check ticket for user.
     *
     * @param string $ticketToken
     * @return void
     */
    public function checkTicket($ticketToken)
    {
        $this->db->select('*')->from('tickets')->where('tickets.token', $ticketToken);
        $query = $this->db->get();
        if (!$query->num_rows()) {
            show_error('You haven\'t ticket on this seans.');
        }
    }
}