<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Rating model.
 */
class model_rating extends CI_Model
{
    /**
     * Rating format.
     *
     * @var array
     */
    protected $ratingFormat = ['one', 'two', 'three', 'four', 'five'];

    /**
     * Prepare rating format.
     *
     * @param int $ratingValue
     * @return string
     */
    public function prepareRatingFormat($ratingValue) {
        return isset($this->ratingFormat[--$ratingValue]) ? $this->ratingFormat[$ratingValue] : 'zero';
    }
}