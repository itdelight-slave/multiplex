-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Дек 28 2015 г., 16:06
-- Версия сервера: 5.6.24
-- Версия PHP: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `slavpeople`
--

-- --------------------------------------------------------

--
-- Структура таблицы `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `id` int(11) NOT NULL,
  `file_type_id` varchar(255) NOT NULL,
  `resource_id` int(11) NOT NULL,
  `mime_type` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status_id` int(11) NOT NULL,
  `film_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `files`
--

INSERT INTO `files` (`id`, `file_type_id`, `resource_id`, `mime_type`, `name`, `status_id`, `film_id`) VALUES
(1, '2', 1, 'jpg', 'slide-1', 1, 1),
(2, '2', 1, 'jpg', 'film-1', 1, 2),
(3, '2', 1, 'jpg', 'film-2', 1, 3),
(4, '6', 1, 'mp4', 'video', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `file_type`
--

CREATE TABLE IF NOT EXISTS `file_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `file_type`
--

INSERT INTO `file_type` (`id`, `name`) VALUES
(1, 'poster'),
(2, 'general_poster'),
(3, 'soundtrack'),
(4, 'cadr'),
(5, 'trailer'),
(6, 'film');

-- --------------------------------------------------------

--
-- Структура таблицы `films`
--

CREATE TABLE IF NOT EXISTS `films` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `genre_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `rating_id` int(11) NOT NULL,
  `rating_value` int(11) NOT NULL,
  `date_premiere` date NOT NULL,
  `date_end_translation` date NOT NULL,
  `film_info_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `films`
--

INSERT INTO `films` (`id`, `name`, `genre_id`, `file_id`, `rating_id`, `rating_value`, `date_premiere`, `date_end_translation`, `film_info_id`) VALUES
(1, 'Звездные войны', 1, 2, 3, 5, '2015-12-22', '2015-12-31', 1),
(2, '45 лет', 1, 3, 3, 4, '2015-12-27', '2015-12-31', 2),
(3, 'Спектр', 1, 4, 3, 3, '2015-12-31', '2016-12-31', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `film_info`
--

CREATE TABLE IF NOT EXISTS `film_info` (
  `id` int(11) NOT NULL,
  `cast` varchar(512) NOT NULL,
  `scenario` varchar(255) NOT NULL,
  `producer` varchar(255) NOT NULL,
  `operator` varchar(255) NOT NULL,
  `composer` varchar(255) NOT NULL,
  `painter` varchar(255) NOT NULL,
  `installation` varchar(255) NOT NULL,
  `budget` int(11) NOT NULL,
  `age` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `original_name` varchar(255) NOT NULL,
  `year` varchar(255) NOT NULL,
  `subname` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `film_info`
--

INSERT INTO `film_info` (`id`, `cast`, `scenario`, `producer`, `operator`, `composer`, `painter`, `installation`, `budget`, `age`, `description`, `original_name`, `year`, `subname`) VALUES
(1, 'cast', 'scenario1, scenario2', 'prod1, prod2', 'operator1,oper2', 'composer1,comp2', 'painter1, painter2', 'instalation1, inst2', 120000, '16', 'a lot ot text', 'Star wars', '2015', 'Пробуждение силы'),
(2, 'cast', 'scenario1, scenario2', 'prod1, prod2', 'operator1,oper2', 'composer1,comp2', 'painter1, painter2', 'instalation1, inst2', 120000, '16', 'a lot ot text', '45 years', '2015', 'subname'),
(3, 'cast', 'scenario1, scenario2', 'prod1, prod2', 'operator1,oper2', 'composer1,comp2', 'painter1, painter2', 'instalation1, inst2', 120000, '16', 'a lot ot text', 'Spectr', '2015', 'subname');

-- --------------------------------------------------------

--
-- Структура таблицы `genre`
--

CREATE TABLE IF NOT EXISTS `genre` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `genre`
--

INSERT INTO `genre` (`id`, `name`, `description`) VALUES
(2, 'test', 'qwert');

-- --------------------------------------------------------

--
-- Структура таблицы `rating`
--

CREATE TABLE IF NOT EXISTS `rating` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `resource`
--

CREATE TABLE IF NOT EXISTS `resource` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `reviews`
--

CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `reviews` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `reviews`
--

INSERT INTO `reviews` (`id`, `user_id`, `reviews`) VALUES
(1, 12, 'Loren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren Ipsum'),
(2, 21, 'Loren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren Ipsum'),
(3, 12, 'Loren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren Ipsum'),
(4, 21, 'Loren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren IpsumLoren Ipsum');

-- --------------------------------------------------------

--
-- Структура таблицы `room`
--

CREATE TABLE IF NOT EXISTS `room` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `room_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `room_type`
--

CREATE TABLE IF NOT EXISTS `room_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `seans`
--

CREATE TABLE IF NOT EXISTS `seans` (
  `id` int(11) NOT NULL,
  `date_time_seans` datetime NOT NULL,
  `film_id` int(11) NOT NULL,
  `room_id` int(11) NOT NULL,
  `price` int(11) DEFAULT NULL,
  `free_count` int(11) NOT NULL,
  `tikets_count` int(11) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `seans`
--

INSERT INTO `seans` (`id`, `date_time_seans`, `film_id`, `room_id`, `price`, `free_count`, `tikets_count`, `status`) VALUES
(1, '2015-12-22 22:20:00', 1, 1, 150, 10, 10, -1),
(2, '2015-12-22 21:20:00', 1, 1, 160, 10, 10, -1),
(3, '2015-12-22 23:20:00', 1, 1, 150, 12, 12, -1),
(4, '2015-12-22 20:20:00', 1, 1, 160, 12, 12, -1),
(5, '2015-12-23 23:20:00', 1, 1, 150, 12, 12, -1),
(6, '2015-12-23 20:20:00', 1, 1, 160, 12, 12, -1),
(7, '2015-12-23 21:20:00', 1, 1, 160, 12, 12, -1),
(8, '2015-12-23 22:20:00', 1, 1, 160, 12, 12, -1),
(9, '2015-12-26 23:05:11', 1, 1, 123, 12, 12, 1),
(10, '2015-12-28 13:53:18', 1, 1, 123, 8, 12, 0),
(11, '2015-12-28 17:49:20', 1, 1, 23, 10, 12, 0),
(12, '2015-12-28 18:49:20', 1, 1, 23, 12, 12, 0),
(13, '2015-12-28 19:49:20', 1, 1, 23, 12, 12, 0),
(14, '2015-12-28 19:49:20', 2, 1, 23, 12, 12, 0),
(15, '2015-12-31 19:49:20', 3, 1, 23, 12, 12, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `id` int(11) NOT NULL,
  `success` tinyint(1) NOT NULL,
  `fail` tinyint(1) NOT NULL,
  `onhold` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `tickets`
--

CREATE TABLE IF NOT EXISTS `tickets` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `seans_id` int(11) NOT NULL,
  `date_buy` datetime NOT NULL,
  `token` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `tickets`
--

INSERT INTO `tickets` (`id`, `user_id`, `seans_id`, `date_buy`, `token`) VALUES
(1, 1, 9, '2015-12-26 22:48:34', '0aa1883c6411f7873cb83dacb17b0afc'),
(3, 1, 10, '2015-12-27 11:42:33', 'c6bff625bdb0393992c9d4db0c6bbe45'),
(4, 1, 10, '2015-12-27 11:42:53', 'c6bff625bdb0393992c9d4db0c6bbe45'),
(5, 1, 10, '2015-12-27 18:02:24', 'c6bff625bdb0393992c9d4db0c6bbe45'),
(6, 1, 11, '2015-12-28 15:37:29', 'b59c67bf196a4758191e42f76670ceba'),
(7, 1, 11, '2015-12-28 15:38:04', 'b59c67bf196a4758191e42f76670ceba');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `file_type`
--
ALTER TABLE `file_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `films`
--
ALTER TABLE `films`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `film_info`
--
ALTER TABLE `film_info`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `genre`
--
ALTER TABLE `genre`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `resource`
--
ALTER TABLE `resource`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `room_type`
--
ALTER TABLE `room_type`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `seans`
--
ALTER TABLE `seans`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `files`
--
ALTER TABLE `files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `file_type`
--
ALTER TABLE `file_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `films`
--
ALTER TABLE `films`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `film_info`
--
ALTER TABLE `film_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `genre`
--
ALTER TABLE `genre`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `rating`
--
ALTER TABLE `rating`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `resource`
--
ALTER TABLE `resource`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `room`
--
ALTER TABLE `room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `room_type`
--
ALTER TABLE `room_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `seans`
--
ALTER TABLE `seans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT для таблицы `status`
--
ALTER TABLE `status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
