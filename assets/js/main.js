﻿$(document).ready(function () {
    // main slider
	$('.main-slider').slick({
		infinite: true,
		fade: true,
		arrows: false,
		dots: true,
		slidesToShow: 1,
		slidesToScroll: 1
	});


	// popups	
	$(document).on("click", ".buy-tickets-btn", function () {
        $('.popup.buy-tickets').addClass("_active_popup");
    });

	$(document).on("click", ".close-popup", function () {
        $(this).parents('.popup').removeClass("_active_popup");
    });
});
